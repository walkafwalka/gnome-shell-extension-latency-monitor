/* exported init */
/* exported buildPrefsWidget */

"use strict";

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();

const { Gio, GObject, Gtk } = imports.gi;

const LatencyMonitorPrefsWidget = GObject.registerClass(
	class LatencyMonitorPrefsWidget extends Gtk.Notebook {
		_init() {
			super._init();
			this.settings = ExtensionUtils.getSettings(Extension.metadata["settings-schema"]);

			this.builder = new Gtk.Builder();
			this.builder.add_from_file(Extension.path + "/settings.ui");

			let object, settingKey;
			for (settingKey of this.settings["settings-schema"].list_keys()) {
				object = this.builder.get_object(settingKey);

				this.setSetting(object);

				switch (object.constructor) {
					case Gtk.Entry:
						this.settings.bind(settingKey, object.buffer, "text", Gio.SettingsBindFlags.DEFAULT);
						object.connect("show", this.handleResetIcon.bind(this));
						object.connect("changed", this.handleResetIcon.bind(this));
						object.connect("icon-press", this.resetSetting.bind(this));
						break;

					case Gtk.SpinButton:
						this.settings.bind(settingKey, object, "value", Gio.SettingsBindFlags.DEFAULT);
						break;
				}
			}

			this.append_page(this.builder.get_object("box-latency"), Gtk.Label.new("Latency"));
		}

		handleResetIcon(object) {
			const objectName = object.get_name();
			const settingDefaultValue = this.settings.get_default_value(objectName);

			let iconName = "document-revert";
			if (object.buffer.text === settingDefaultValue.get_string()[0]) iconName = null;
			object.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, iconName);
		}

		resetSetting(object) {
			const objectName = object.get_name();

			this.settings.reset(objectName);
			this.setSetting(object);
		}

		setSetting(object) {
			const objectName = object.get_name();

			switch (object.constructor) {
				case Gtk.Entry:
					object.buffer.text = this.settings.get_string(objectName);
					break;

				case Gtk.SpinButton:
					object.set_value(this.settings.get_int(objectName));
					break;
			}
		}
	}
);


function init() {
	// do nothing
}

function buildPrefsWidget() {
	const prefsWidget = new LatencyMonitorPrefsWidget();
	prefsWidget.show_all();
	return prefsWidget;
}
