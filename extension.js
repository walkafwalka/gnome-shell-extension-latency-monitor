/* exported init */
/* exported enable */
/* exported disable */

"use strict";

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();

const { Clutter, Gio, GLib, GObject, St } = imports.gi;
const { main, panelMenu } = imports.ui;

const LatencyIndicator = GObject.registerClass(
	class LatencyIndicator extends panelMenu.Button {
		_init() {
			super._init(St.Align.START, "LatencyIndicator", false);
			this.settings = ExtensionUtils.getSettings(Extension.metadata["settings-schema"]);

			this.icon = new St.Icon({ style_class: "system-status-icon" });
			this.label = new St.Label({
				text: "...",
				y_align: Clutter.ActorAlign.CENTER,
				y_expand: true
			});
			this._update();

			this.box = new St.BoxLayout();
			this.box.add(this.icon);
			this.box.add(this.label);

			this.add_child(this.box);
		}

		_update() {
			const labelIcon = this.settings.get_string("label-icon");
			const pingCount = this.settings.get_int("ping-count");
			const pingDestination = this.settings.get_string("ping-destination");
			const labelRefreshDelay = this.settings.get_int("label-refresh-delay");

			if (labelIcon) this.icon.gicon = new Gio.ThemedIcon({ name: labelIcon });
			else this.icon.gicon = null;

			try {
				const argv = ["ping", "-c", pingCount.toString(), pingDestination];
				const flags = Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE;
				const process = Gio.Subprocess.new(argv, flags);

				process.communicate_utf8_async(null, null, (process, result) => {
					try {
						const [, stdout, stderr] = process.communicate_utf8_finish(result);
						const exitStatus = process.get_exit_status();

						const EXIT_STATUS_TIMEOUT = 1;
						const EXIT_STATUS_ERROR = 2;

						switch (exitStatus) {
							case EXIT_STATUS_TIMEOUT:
								this.label.text = "Timeout";
								throw new Error(stdout);
							case EXIT_STATUS_ERROR:
								this.label.text = "Error";
								throw new Error(stderr);
						}

						this.label.text = this.getPingRTT(stdout);
					} catch (error) {
						logError(error);
					} finally {
						this._timeout = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, labelRefreshDelay, this._update.bind(this));
					}
				});
			} catch (error) {
				logError(error);
			}

			return GLib.SOURCE_REMOVE;
		}

		_onDestroy() {
			if (this._timeout) GLib.source_remove(this._timeout);
			super._onDestroy();
		}

		getPingRTT(pingResult) {
			pingResult = pingResult.trim().split(/\n/);
			pingResult = pingResult.pop().toString();          // rtt min/avg/max/mdev = 13.109/21.812/38.923/9.078 ms
			pingResult = pingResult.split(/\s/)[3].toString(); // 13.109/21.812/38.923/9.078
			pingResult = pingResult.split(/\//);

			/* eslint-disable sort-keys */
			const pingRTT = {
				min: Math.round(pingResult[0]),
				avg: Math.round(pingResult[1]),
				max: Math.round(pingResult[2]),
				mdev: Math.round(pingResult[3]),
			};
			/* eslint-enable sort-keys */

			const labelFormat = this.settings.get_string("label-format");
			let label = labelFormat;

			const matches = label.matchAll(/{(.*?)}/g);
			for (const match of matches) {
				if (["min", "avg", "max", "mdev"].includes(match[1])) {
					label = label.replace(match[0], pingRTT[match[1]]);
				}
			}

			return label;
		}
	}
);


let latencyIndicator;

function init() {
	// do nothing
}

function enable() {
	latencyIndicator = new LatencyIndicator();
	main.panel.addToStatusArea("latencyIndicator", latencyIndicator);
}

function disable() {
	latencyIndicator.destroy();
}
