#!/usr/bin/env sh
set -e

NAME=$1
VERSION=$2

# Set version in metadata.json
sed -i "s/0.0.0-development/${VERSION}/g" metadata.json

# Create ZIP archive
zip -q -r "${NAME}-${VERSION}.zip" -- metadata.json settings.ui *.js schemas/
